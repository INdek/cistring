#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};
use std::{
    fmt,
    cmp::{Eq, PartialEq},
    hash::{Hash, Hasher},
    ops::Deref,
    str::FromStr,
    borrow::Borrow,
    string::{FromUtf8Error, FromUtf16Error},
    ops::RangeBounds,
};

#[derive(Clone, Debug, Default, PartialOrd, Ord, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(transparent))]
pub struct CiString(String);

impl CiString {
    pub const fn new() -> Self {
        Self(String::new())
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self(String::with_capacity(capacity))
    }

    pub fn from_utf8(vec: Vec<u8>) -> Result<Self, FromUtf8Error> {
        String::from_utf8(vec)
            .map(Self)
    }

    // TODO: From_utf8_lossy
    
    pub fn from_utf16(vec: &[u16]) -> Result<Self, FromUtf16Error> {
        String::from_utf16(vec)
            .map(Self)
    }

    pub fn from_utf16_lossy(v: &[u16]) -> Self {
        Self(String::from_utf16_lossy(v))
    }

    // TODO: into raw parts
    // TODO: from raw parts

    pub unsafe fn from_utf8_unchecked(bytes: Vec<u8>) -> Self {
        Self(String::from_utf8_unchecked(bytes))
    }

    pub fn into_bytes(self) -> Vec<u8> {
        self.0.into_bytes()
    }

    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }

    pub fn as_mut_str(&mut self) -> &str {
        self.0.as_mut_str()
    }

    pub fn push_str(&mut self, string: &str) {
        self.0.push_str(string)
    }

    pub fn capacity(&self) -> usize {
        self.0.capacity()
    }
    
    pub fn reserve(&mut self, additional: usize) {
        self.0.reserve(additional)
    }
 
    pub fn reserve_exact(&mut self, additional: usize) {
        self.0.reserve_exact(additional)
    }

    // TODO: try_reserve
    // TODO: try_reserve_exact

    pub fn shrink_to_fit(&mut self) {
        self.0.shrink_to_fit()
    }

    // TODO: shrink_to
    
    pub fn push(&mut self, ch: char) {
        self.0.push(ch)
    }

    pub fn as_bytes(&self) -> &[u8] {
        self.0.as_bytes()
    }

    pub fn truncate(&mut self, new_len: usize) {
        self.0.truncate(new_len)
    }

    pub fn pop(&mut self) -> Option<char> {
        self.0.pop()
    }

    pub fn remove(&mut self, idx: usize) -> char {
        self.0.remove(idx)
    }

    pub fn retain<F>(&mut self, f: F)
    where F: FnMut(char) -> bool{
        self.0.retain(f)
    }

    pub fn insert(&mut self, idx: usize, ch: char) {
        self.0.insert(idx, ch)
    }

    pub unsafe fn as_mut_vec(&mut self) -> &mut Vec<u8> {
        self.0.as_mut_vec()
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn split_off(&mut self, at: usize) -> Self {
        Self::from(self.0.split_off(at))
    }

    pub fn clear(&mut self) {
        self.0.clear()
    }

    // TODO: drain

    pub fn replace_range<R>(&mut self, range: R, replace_with: &str)
    where R: RangeBounds<usize> {
        self.0.replace_range(range, replace_with)
    }

    pub fn into_boxed_str(self) -> Box<str> {
        self.0.into_boxed_str()
    }
}

impl fmt::Display for CiString {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl PartialEq for CiString {
    fn eq(&self, other: &CiString) -> bool {
        self.0.to_lowercase() == other.0.to_lowercase()
    }
}

impl PartialEq<String> for CiString {
    fn eq(&self, other: &String) -> bool {
        self.0.to_lowercase() == other.to_lowercase()
    }
}
impl PartialEq<CiString> for String {
    fn eq(&self, other: &CiString) -> bool {
        other.0.to_lowercase() == self.to_lowercase()
    }
}

impl PartialEq<&str> for CiString {
    fn eq(&self, other: &&str) -> bool {
        self.0.to_lowercase() == other.to_lowercase()
    }
}


impl Hash for CiString {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.0.to_lowercase().hash(hasher);
    }
}

impl AsRef<str> for CiString {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl Borrow<str> for CiString {
    fn borrow(&self) -> &str {
        self.0.borrow()
    }
}

impl Deref for CiString {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl FromStr for CiString {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self::from(s))
    }
}

impl Into<String> for CiString {
    fn into(self) -> String {
        self.0
    }
}

impl From<String> for CiString {
    fn from(value: String) ->  Self {
        CiString(value)
    }
}

impl From<&str> for CiString {
    fn from(value: &str) ->  Self {
        CiString(value.into())
    }
}


#[cfg(test)]
mod tests {
    use proptest::prelude::*;
    use super::*;

    proptest! {
        #[test]
        fn case_insensitive_compares_with_itself(s in "\\PC*") {
            let ci = CiString::from(s.clone());
            let ci_lower = CiString::from(s.to_lowercase());
            
            prop_assert_eq!(&ci, &ci);
            prop_assert_eq!(&ci, &ci_lower);
        }
        
        #[test]
        fn case_insensitive_compares_with_string(s in "\\PC*") {
            let ci = CiString::from(s.clone());
            let ci_lower = CiString::from(s.to_lowercase());
            let s_lower = s.to_lowercase();
            
            prop_assert_eq!(&ci, &s);
            prop_assert_eq!(&ci, &s_lower);
            prop_assert_eq!(&s_lower, &ci_lower);
            prop_assert_eq!(&ci_lower, &s_lower);
        }
    }
}
