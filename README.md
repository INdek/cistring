# CiString

A string type that preserve case, but compares insensitiveley.

Example:
```
let upper = CiString::from("HELLO WORLD!");
let lower = CiString::from("hello world!");

assert_eq!(upper, lower);
println!("{}", upper); // -> "HELLO WORLD!"
println!("{}", lower); // -> "hello world!"
```

### License

This project is licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or
   http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or
   http://opensource.org/licenses/MIT)

at your option.